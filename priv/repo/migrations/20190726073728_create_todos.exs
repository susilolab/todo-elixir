defmodule Todo.Repo.Migrations.CreateTodos do
  use Ecto.Migration

  def change do
    create table(:todos) do
      add :title, :string
      add :body, :string
      add :user_id, :integer 
      add :category_id, :integer

      timestamps()
    end

  end
end
