defmodule Todo.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :string, size: 50
      add :encrypted_password, :string, size: 256
      add :photo, :string, size: 200

      timestamps()
    end

    create unique_index(:users, [:username])
  end
end
