defmodule Todo.Repo.Migrations.CreateContactUs do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:contact_us) do
      add :name, :string, size: 50
      add :email, :string, size: 50
      add :message, :string, size: 256

      timestamps()
    end

  end
end
