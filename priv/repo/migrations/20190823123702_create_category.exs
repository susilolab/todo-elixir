defmodule Todo.Repo.Migrations.CreateCategory do
  use Ecto.Migration

  def change do
    create table(:category) do
      add :name, :string, size: 50
      add :publish, :integer, size: 1

      timestamps()
    end

  end
end
