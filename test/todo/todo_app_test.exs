defmodule Todo.TodoAppTest do
  require Logger
  use Todo.DataCase

  alias Todo.TodoApp

  describe "todos" do
    alias Todo.TodoApp.Todos

    @valid_attrs %{body: "some body", title: "some title", category_id: 1}
    @update_attrs %{body: "some updated body", title: "some updated title"}
    @invalid_attrs %{body: nil, title: nil}

    def todos_fixture(attrs \\ %{}) do
      {:ok, todos} =
        attrs
        |> Enum.into(@valid_attrs)
        |> TodoApp.create_todos()

      todos
    end

    test "list_todos/0 returns all todos" do
      todos = todos_fixture()
      assert TodoApp.list_todos() == [todos]
    end

    test "get_todos!/1 returns the todos with given id" do
      todos = todos_fixture()
      assert TodoApp.get_todos!(todos.id) == todos
    end

    test "create_todos/1 with valid data creates a todos" do
      assert {:ok, %Todos{} = todos} = TodoApp.create_todos(@valid_attrs)
      assert todos.body == "some body"
      assert todos.title == "some title"
    end

    test "create_todos/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = TodoApp.create_todos(@invalid_attrs)
    end

    test "update_todos/2 with valid data updates the todos" do
      todos = todos_fixture()
      assert {:ok, %Todos{} = todos} = TodoApp.update_todos(todos, @update_attrs)
      assert todos.body == "some updated body"
      assert todos.title == "some updated title"
    end

    test "update_todos/2 with invalid data returns error changeset" do
      todos = todos_fixture()
      assert {:error, %Ecto.Changeset{}} = TodoApp.update_todos(todos, @invalid_attrs)
      assert todos == TodoApp.get_todos!(todos.id)
    end

    test "delete_todos/1 deletes the todos" do
      todos = todos_fixture()
      assert {:ok, %Todos{}} = TodoApp.delete_todos(todos)
      assert_raise Ecto.NoResultsError, fn -> TodoApp.get_todos!(todos.id) end
    end

    test "change_todos/1 returns a todos changeset" do
      todos = todos_fixture()
      assert %Ecto.Changeset{} = TodoApp.change_todos(todos)
    end
  end

  describe "contact_us" do
    alias Todo.TodoApp.ContactUs

    @valid_attrs %{name: "Agus", email: "smartgdi@gmail.com", message: "Hello"}
    @update_attrs %{name: "Agus Susilo", email: "smartgdi_@gmail.com"}
    @invalid_attrs %{name: nil, email: nil, message: nil}

    def contact_us_fixture(attrs \\ %{}) do
      {:ok, contact_us} =
        attrs
        |> Enum.into(@valid_attrs)
        |> TodoApp.create_contact_us()

      contact_us
    end

    test "list_contact_us/0 returns all contact_us" do
      contact_us = contact_us_fixture()
      assert TodoApp.list_contact_us() == [contact_us]
    end

    test "get_contact_us!/1 returns the contact_us with given id" do
      contact_us = contact_us_fixture()
      assert TodoApp.get_contact_us!(contact_us.id) == contact_us
    end

    test "create_contact_us/1 with valid data creates a contact_us" do
      assert {:ok, %ContactUs{} = contact_us} = TodoApp.create_contact_us(@valid_attrs)
    end

    test "create_contact_us/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = TodoApp.create_contact_us(@invalid_attrs)
    end

    test "update_contact_us/2 with valid data updates the contact_us" do
      contact_us = contact_us_fixture()
      assert {:ok, %ContactUs{} = contact_us} = TodoApp.update_contact_us(contact_us, @update_attrs)
    end

    test "update_contact_us/2 with invalid data returns error changeset" do
      contact_us = contact_us_fixture()
      Logger.debug "#{inspect contact_us}"
      assert {:error, %Ecto.Changeset{}} = TodoApp.update_contact_us(contact_us, @invalid_attrs)
      assert contact_us == TodoApp.get_contact_us!(contact_us.id)
    end

    test "delete_contact_us/1 deletes the contact_us" do
      contact_us = contact_us_fixture()
      assert {:ok, %ContactUs{}} = TodoApp.delete_contact_us(contact_us)
      assert_raise Ecto.NoResultsError, fn -> TodoApp.get_contact_us!(contact_us.id) end
    end

    test "change_contact_us/1 returns a contact_us changeset" do
      contact_us = contact_us_fixture()
      assert %Ecto.Changeset{} = TodoApp.change_contact_us(contact_us)
    end
  end

  describe "category" do
    alias Todo.TodoApp.Category

    @valid_attrs %{name: "some name", publish: 1}
    @update_attrs %{name: "some updated name", publish: 1}
    @invalid_attrs %{name: nil, publish: nil}

    def category_fixture(attrs \\ %{}) do
      {:ok, category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> TodoApp.create_category()

      category
    end

    test "list_category/0 returns all category" do
      category = category_fixture()
      assert TodoApp.list_category() == [category]
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert TodoApp.get_category!(category.id) == category
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = TodoApp.create_category(@valid_attrs)
      assert category.name == "some name"
      assert category.publish == 1
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = TodoApp.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, %Category{} = category} = TodoApp.update_category(category, @update_attrs)
      assert category.name == "some updated name"
      assert category.publish == 1
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = TodoApp.update_category(category, @invalid_attrs)
      assert category == TodoApp.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      assert {:ok, %Category{}} = TodoApp.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> TodoApp.get_category!(category.id) end
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = TodoApp.change_category(category)
    end
  end
end
