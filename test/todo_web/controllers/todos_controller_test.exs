defmodule TodoWeb.TodosControllerTest do
  use TodoWeb.ConnCase

  alias Todo.TodoApp

  @create_attrs %{body: "some body", title: "some title", category_id: 1, user_id: 1}
  @update_attrs %{body: "some updated body", title: "some updated title"}
  @invalid_attrs %{body: nil, title: nil}

  def fixture(:todos) do
    {:ok, todos} = TodoApp.create_todos(@create_attrs)
    todos
  end

  describe "index" do
    test "lists all todos", %{conn: conn} do
      conn = get(conn, Routes.todos_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Todos"
    end
  end

  describe "new todos" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.todos_path(conn, :new))
      assert html_response(conn, 200) =~ "New Todos"
    end
  end

  describe "create todos" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.todos_path(conn, :create), todos: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.todos_path(conn, :show, id)

      conn = get(conn, Routes.todos_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Todos"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.todos_path(conn, :create), todos: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Todos"
    end
  end

  describe "edit todos" do
    setup [:create_todos]

    test "renders form for editing chosen todos", %{conn: conn, todos: todos} do
      conn = get(conn, Routes.todos_path(conn, :edit, todos))
      assert html_response(conn, 200) =~ "Edit Todos"
    end
  end

  describe "update todos" do
    setup [:create_todos]

    test "redirects when data is valid", %{conn: conn, todos: todos} do
      conn = put(conn, Routes.todos_path(conn, :update, todos), todos: @update_attrs)
      assert redirected_to(conn) == Routes.todos_path(conn, :show, todos)

      conn = get(conn, Routes.todos_path(conn, :show, todos))
      assert html_response(conn, 200) =~ "some updated body"
    end

    test "renders errors when data is invalid", %{conn: conn, todos: todos} do
      conn = put(conn, Routes.todos_path(conn, :update, todos), todos: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Todos"
    end
  end

  describe "delete todos" do
    setup [:create_todos]

    test "deletes chosen todos", %{conn: conn, todos: todos} do
      conn = delete(conn, Routes.todos_path(conn, :delete, todos))
      assert redirected_to(conn) == Routes.todos_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.todos_path(conn, :show, todos))
      end
    end
  end

  defp create_todos(_) do
    todos = fixture(:todos)
    {:ok, todos: todos}
  end
end
