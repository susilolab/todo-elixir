defmodule Todo.ExcelExporter do
  use GenServer
  require Logger
  alias Elixlsx.Workbook
  alias Elixlsx.Sheet
  # alias Todo.TodoApp

  @moduledoc """
  The TodoApp context.
  """
  @name __MODULE__
  
  # client
  def start_link(args) do
    GenServer.start_link(@name, args, name: @name)
  end
  
  def export(user_id) do
    GenServer.cast(@name, {:export_xls, user_id})
  end

  # server
  def init(initial_value) do
    Logger.info("ExcelExporter berjalan..")
    {:ok, initial_value}
  end

  def handle_cast({:export_xls, args}, state) do
    # result = TodoApp.list_todos_by_id(args)
    uuid = UUID.uuid4()
    # sheet1 = 
    Workbook.append_sheet(
      %Workbook{}, 
      Sheet.with_name("Sheet1") |> Sheet.set_cell("A1", "Hello", bold: true)
    ) |> Elixlsx.write_to("priv/static/hello-#{uuid}.xlsx")

    {:noreply, [args | state]}
  end
  
  def handle_info(_, state) do
    {:noreply, state}
  end
end
