defmodule Todo.Repo do
  use Ecto.Repo,
    otp_app: :todo,
    adapter: Ecto.Adapters.MyXQL

  use Scrivener, page_size: 20
end
