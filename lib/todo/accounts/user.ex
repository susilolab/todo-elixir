defmodule Todo.Accounts.User do
  @moduledoc """
  User struct memetakan kolom pada tabel user
  """

  use Ecto.Schema
  import Ecto.Changeset
  # alias Todo.Accounts.User
  alias Bcrypt

  schema "users" do
    field :encrypted_password, :string
    field :username, :string
    field :photo, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :encrypted_password, :photo, :password])
    |> validate_length(:username, min: 3, max: 50)
    |> validate_length(:encrypted_password, min: 8, max: 256)
    |> validate_length(:password, min: 8, max: 256)
    |> validate_length(:password_confirmation, min: 8, max: 256)
    |> validate_required([:username, :encrypted_password, :password])
    |> validate_confirmation(:password)
    |> unique_constraint(:username)
    |> update_change(:encrypted_password, &Bcrypt.hash_pwd_salt/1)
    |> update_change(:username, &String.downcase/1)
  end

  @doc """
  Update photo changeset agar field photo diwajibkan diisi
  """
  def update_photo(user, attrs) do
    user
    |> cast(attrs, [:photo])
    |> validate_required([:photo])
  end
end
