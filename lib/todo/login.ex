defmodule Todo.Login do
  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :username
    field :password
  end

  def changeset(login, attrs) do
    login
    |> cast(attrs, [:username, :password])
    |> validate_required([:username, :password])
    |> validate_length(:password, min: 6, max: 256)
  end
end
