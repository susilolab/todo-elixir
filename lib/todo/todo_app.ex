defmodule Todo.TodoApp do
  @moduledoc """
  The TodoApp context.
  """

  import Ecto.Query, warn: false
  alias Todo.Repo

  alias Todo.TodoApp.Todos

  @doc """
  Returns the list of todos.

  ## Examples

      iex> list_todos()
      [%Todos{}, ...]

  """
  def list_todos do
    Repo.all(Todos)
  end

  def list_todos_by_id(id, params) do
    config = Todos.filter_options(:admin)
    filter_params = Map.drop(params, ~w(page_size page_number page fields expand))
    {:ok, filter } = Filtrex.parse_params(config, filter_params)

    fields =
      if Map.has_key?(params, "fields") do
        filter_fields(params["fields"])
      else
        Todos.__schema__(:fields)
      end

    base_query = from(t in Todos, where: t.user_id == ^id)
    base_query
    |> select([t], map(t, ^fields))
    |> preload(:category)
    |> Filtrex.query(filter)
    |> Repo.paginate(params)
  end

  # Filter params field agar bisa memilih field apa saja yg ingin ditampilkan
  defp filter_fields(params) do
    if String.contains?(params, ",") do
      String.split(params, ",", trim: true) |> Enum.map(fn x -> String.to_atom(x) end)
    else
      [String.to_atom(params)]
    end
  end

  @doc """
  Gets a single todos.

  Raises `Ecto.NoResultsError` if the Todos does not exist.

  ## Examples

      iex> get_todos!(123)
      %Todos{}

      iex> get_todos!(456)
      ** (Ecto.NoResultsError)

  """
  def get_todos!(id), do: Repo.get!(Todos, id)

  @doc """
  Creates a todos.

  ## Examples

      iex> create_todos(%{field: value})
      {:ok, %Todos{}}

      iex> create_todos(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_todos(attrs \\ %{}) do
    %Todos{}
    |> Todos.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a todos.

  ## Examples

      iex> update_todos(todos, %{field: new_value})
      {:ok, %Todos{}}

      iex> update_todos(todos, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_todos(%Todos{} = todos, attrs) do
    todos
    |> Todos.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Todos.

  ## Examples

      iex> delete_todos(todos)
      {:ok, %Todos{}}

      iex> delete_todos(todos)
      {:error, %Ecto.Changeset{}}

  """
  def delete_todos(%Todos{} = todos) do
    Repo.delete(todos)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking todos changes.

  ## Examples

      iex> change_todos(todos)
      %Ecto.Changeset{source: %Todos{}}

  """
  def change_todos(%Todos{} = todos) do
    Todos.changeset(todos, %{})
  end

  alias Todo.TodoApp.ContactUs

  @doc """
  Returns the list of contact_us.

  ## Examples

      iex> list_contact_us()
      [%ContactUs{}, ...]

  """
  def list_contact_us do
    Repo.all(ContactUs)
  end

  @doc """
  Gets a single contact_us.

  Raises `Ecto.NoResultsError` if the Contact us does not exist.

  ## Examples

      iex> get_contact_us!(123)
      %ContactUs{}

      iex> get_contact_us!(456)
      ** (Ecto.NoResultsError)

  """
  def get_contact_us!(id), do: Repo.get!(ContactUs, id)

  @doc """
  Creates a contact_us.

  ## Examples

      iex> create_contact_us(%{field: value})
      {:ok, %ContactUs{}}

      iex> create_contact_us(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_contact_us(attrs \\ %{}) do
    %ContactUs{}
    |> ContactUs.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a contact_us.

  ## Examples

      iex> update_contact_us(contact_us, %{field: new_value})
      {:ok, %ContactUs{}}

      iex> update_contact_us(contact_us, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_contact_us(%ContactUs{} = contact_us, attrs) do
    contact_us
    |> ContactUs.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ContactUs.

  ## Examples

      iex> delete_contact_us(contact_us)
      {:ok, %ContactUs{}}

      iex> delete_contact_us(contact_us)
      {:error, %Ecto.Changeset{}}

  """
  def delete_contact_us(%ContactUs{} = contact_us) do
    Repo.delete(contact_us)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking contact_us changes.

  ## Examples

      iex> change_contact_us(contact_us)
      %Ecto.Changeset{source: %ContactUs{}}

  """
  def change_contact_us(%ContactUs{} = contact_us) do
    ContactUs.changeset(contact_us, %{})
  end

  alias Todo.TodoApp.Category

  @doc """
  Returns the list of category.

  ## Examples

      iex> list_category()
      [%Category{}, ...]

  """
  def list_category do
    query = from c in Category, where: c.publish == 1
    Repo.all(query)
  end

  @doc """
  Gets a single category.

  Raises `Ecto.NoResultsError` if the Category does not exist.

  ## Examples

      iex> get_category!(123)
      %Category{}

      iex> get_category!(456)
      ** (Ecto.NoResultsError)

  """
  def get_category!(id), do: Repo.get!(Category, id)

  @doc """
  Creates a category.

  ## Examples

      iex> create_category(%{field: value})
      {:ok, %Category{}}

      iex> create_category(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_category(attrs \\ %{}) do
    %Category{}
    |> Category.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a category.

  ## Examples

      iex> update_category(category, %{field: new_value})
      {:ok, %Category{}}

      iex> update_category(category, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_category(%Category{} = category, attrs) do
    category
    |> Category.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Category.

  ## Examples

      iex> delete_category(category)
      {:ok, %Category{}}

      iex> delete_category(category)
      {:error, %Ecto.Changeset{}}

  """
  def delete_category(%Category{} = category) do
    Repo.delete(category)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking category changes.

  ## Examples

      iex> change_category(category)
      %Ecto.Changeset{source: %Category{}}

  """
  def change_category(%Category{} = category) do
    Category.changeset(category, %{})
  end
end
