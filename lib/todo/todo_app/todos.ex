defmodule Todo.TodoApp.Todos do
  use Ecto.Schema
  import Ecto.Changeset
  import Filtrex.Type.Config
  alias Todo.TodoApp.Category
  alias Todo.Accounts.User

  # @derive {Jason.Encoder, except: [
  #   :__struct__,
  #   :__meta__,
  # ]}
  schema "todos" do
    field :body, :string
    field :title, :string
    # field :user_id, :integer
    # virtual field, keperluan untuk menyimpan data query select_merge
    # field :category_name, :string, virtual: true

    belongs_to :category, Category
    belongs_to :user, User, foreign_key: :user_id
    timestamps()
  end

  @doc false
  def changeset(todos, attrs) do
    todos
    |> cast(attrs, [:title, :body, :user_id, :category_id])
    |> validate_required([:category_id, :title, :body])
  end

  @doc false
  def filter_options(:admin) do
    defconfig do
      number :user_id
      text :title
    end
  end
end
