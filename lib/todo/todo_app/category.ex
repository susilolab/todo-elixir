defmodule Todo.TodoApp.Category do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, except: [
    :__struct__,
    :__meta__,
  ]}
  schema "category" do
    field :name, :string
    field :publish, :integer

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name, :publish])
    |> validate_required([:name, :publish])
  end
end
