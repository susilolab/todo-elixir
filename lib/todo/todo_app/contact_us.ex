defmodule Todo.TodoApp.ContactUs do
  use Ecto.Schema
  import Ecto.Changeset

  schema "contact_us" do
    field :name, :string
    field :email, :string
    field :message, :string

    timestamps()
  end

  @doc false
  def changeset(contact_us, attrs) do
    contact_us
    |> cast(attrs, [:name, :email, :message])
    |> validate_required([:name, :email, :message])
  end
end
