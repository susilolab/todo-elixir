defmodule TodoWeb.InputHelpers do
  @moduledoc """
  Conveniences for translating and building error messages.
  """

  use Phoenix.HTML

  @doc false
  def input(form, field) do
    type = Phoenix.HTML.Form.input_type(form, field)

    wrapper_opts = [class: "form-group"]
    input_opts = [class: "form-control #{state_class(form, field)}"]

    content_tag :div, wrapper_opts do
      label = label(form, field, humanize(field))
      input = apply(Phoenix.HTML.Form, type, [form, field, input_opts])
      error = TodoWeb.ErrorHelpers.error_tag(form, field) || ""
      [label, input, error]
    end
  end

  def state_class(form, field) do
    cond do
      !form.source.action -> ""
      form.errors[field] -> "is-invalid"
      true -> "is-valid"
    end
  end
end
