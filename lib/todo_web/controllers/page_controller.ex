defmodule TodoWeb.PageController do
  use TodoWeb, :controller
  alias Todo.ExcelExporter

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def export_xls(conn, _params) do
    ExcelExporter.export(1)
    text(conn, "export_xls: " <> UUID.uuid4())
  end

  def about(conn, _params) do
    render(conn, "about.html")
  end
end
