defmodule TodoWeb.UserController do
  import Mogrify, only: [open: 1, resize: 2, save: 2]
  use TodoWeb, :controller

  alias Todo.Accounts
  alias Todo.Accounts.User
  plug :check_auth when action in [:profile]

  @thumbnail_size "200x200"

  defp check_auth(conn, _args) do
    if user_id = get_session(conn, :current_user_id) do
      current_user = Accounts.get_user!(user_id)

      conn
      |> assign(:current_user, current_user)
    else
      conn
      |> put_flash(:error, "You need signin")
      |> redirect(to: Routes.page_path(conn, :index))
      |> halt()
    end
  end

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    new_user_params = 
      if upload = user_params["photo"] do
        extname = Path.extname(upload.filename)
        x = :crypto.hash(:md5, String.replace(upload.filename, extname, "")) 
          |> Base.encode16(case: :lower)
        File.cp(upload.path, "./priv/static/images/#{x}#{extname}")
        Map.put(user_params, "photo", x <> extname)
      else
        user_params
      end
    
    case Accounts.create_user(new_user_params) do
      {:ok, user} ->
        photo = user.photo
        open("./priv/static/images/#{photo}") |> resize(@thumbnail_size) |> save([in_place: true])
        
        conn
        |> put_session(:current_user_id, user.id)
        |> put_flash(:info, "User signup successfully.")
        |> redirect(to: Routes.todos_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    render(conn, "edit_profile.html", user: user, changeset: changeset)
  end

  def update_photo(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)
    new_user_params = 
      if upload = user_params["photo"] do
        extname = Path.extname(upload.filename)
        x = :crypto.hash(:md5, String.replace(upload.filename, extname, "")) 
          |> Base.encode16(case: :lower)
        File.cp(upload.path, "./priv/static/images/#{x}#{extname}")
        Map.put(user_params, "photo", x <> extname)
      else
        user_params
      end

    old_photo = user.photo
    case Accounts.update_user_photo(user, new_user_params) do
      {:ok, user} ->
        File.rm!("./priv/static/images/#{old_photo}")

        photo = user.photo
        open("./priv/static/images/#{photo}") |> resize(@thumbnail_size) |> save([in_place: true])
        
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: Routes.user_path(conn, :profile))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit_profile.html", user: user, changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_user(user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: Routes.user_path(conn, :show))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    {:ok, _user} = Accounts.delete_user(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: Routes.user_path(conn, :index))
  end

  def profile(conn, _params) do
    user_id = get_session(conn, :current_user_id)
    user = Accounts.get_user!(user_id)
    render(conn, "view.html", user: user)
  end
end
