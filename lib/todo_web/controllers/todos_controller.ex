defmodule TodoWeb.TodosController do
  use TodoWeb, :controller

  alias Todo.TodoApp
  alias Todo.TodoApp.Todos
  alias Todo.Accounts
  alias Elixlsx.Sheet
  alias Elixlsx.Workbook

  plug :check_auth when action in [:index, :new, :create, :edit, :update, :delete]

  defp check_auth(conn, _args) do
    if user_id = get_session(conn, :current_user_id) do
      current_user = Accounts.get_user!(user_id)

      conn
      |> assign(:current_user, current_user)
    else
      conn
      |> put_flash(:error, "You need signin")
      |> redirect(to: Routes.page_path(conn, :index))
      |> halt()
    end
  end

  defp get_id(conn) do
    get_session(conn, :current_user_id)  
  end

  def index(conn, params) do
    user_id = get_id(conn)
    todos = TodoApp.list_todos_by_id(user_id, params)
    IO.inspect(todos)
    render(conn, "index.html", todos: todos)
  end

  def new(conn, _params) do
    changeset = TodoApp.change_todos(%Todos{})
    category = TodoApp.list_category()
    render(conn, "new.html", changeset: changeset, category: category)
  end

  def create(conn, %{"todos" => todos_params}) do
    category = TodoApp.list_category()
    case TodoApp.create_todos(todos_params) do
      {:ok, todos} ->
        conn
        |> put_flash(:info, "Todos created successfully.")
        |> redirect(to: Routes.todos_path(conn, :show, todos))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, category: category)
    end
  end

  def show(conn, %{"id" => id}) do
    todos = TodoApp.get_todos!(id)
    render(conn, "show.html", todos: todos)
  end

  def edit(conn, %{"id" => id}) do
    todos = TodoApp.get_todos!(id)
    changeset = TodoApp.change_todos(todos)
    category = TodoApp.list_category()
    render(conn, "edit.html", todos: todos, changeset: changeset, category: category)
  end

  def update(conn, %{"id" => id, "todos" => todos_params}) do
    todos = TodoApp.get_todos!(id)

    case TodoApp.update_todos(todos, todos_params) do
      {:ok, todos} ->
        conn
        |> put_flash(:info, "Todos updated successfully.")
        |> redirect(to: Routes.todos_path(conn, :show, todos))

      {:error, %Ecto.Changeset{} = changeset} ->
        category = TodoApp.list_category()
        render(conn, "edit.html", todos: todos, changeset: changeset, category: category)
    end
  end

  def delete(conn, %{"id" => id}) do
    todos = TodoApp.get_todos!(id)
    {:ok, _todos} = TodoApp.delete_todos(todos)

    conn
    |> put_flash(:info, "Todos deleted successfully.")
    |> redirect(to: Routes.todos_path(conn, :index))
  end

  def gen_xls(id, rows) do
    sheets = 
      rows
      |> Stream.with_index
      |> Enum.reduce(%Sheet{}, fn({row, index}, acc) ->
        acc 
        |> Sheet.set_at(index, 0, row.id) 
        |> Sheet.set_at(index, 1, row.title) 
        |> Sheet.set_at(index, 2, row.body)
      end)

    file_name = "priv/static/todos-#{id}.xlsx"
    Workbook.append_sheet(%Workbook{}, sheets)
    |> Elixlsx.write_to(file_name)    
  end

  def export_xls(conn, %{"id" => id }) do
    rows = TodoApp.list_todos_by_id(id, %{})
    t = Task.async(fn -> gen_xls(id, rows) end)
    Task.await(t)

    file_name = "priv/static/todos-#{id}.xlsx"
    path = Application.app_dir(:todo, file_name)
    send_download(conn, {:file, path})
  end
end
