defmodule TodoWeb.ContactUsController do
  use TodoWeb, :controller

  alias Todo.TodoApp
  alias Todo.TodoApp.ContactUs
  alias Todo.Accounts
  
  plug :check_auth when action in [:index, :edit, :update, :delete]

  defp check_auth(conn, _args) do
    if user_id = get_session(conn, :current_user_id) do
      current_user = Accounts.get_user!(user_id)

      conn
      |> assign(:current_user, current_user)
    else
      conn
      |> put_flash(:error, "You need signin")
      |> redirect(to: Routes.todos_path(conn, :index))
      |> halt()
    end
  end

  def index(conn, _params) do
    contact_us = TodoApp.list_contact_us()
    render(conn, "index.html", contact_us: contact_us)
  end

  def new(conn, _params) do
    changeset = TodoApp.change_contact_us(%ContactUs{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"contact_us" => contact_us_params}) do
    case TodoApp.create_contact_us(contact_us_params) do
      {:ok, _contact_us} ->
        conn
        |> put_flash(:info, "Contact us created successfully.")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    contact_us = TodoApp.get_contact_us!(id)
    render(conn, "show.html", contact_us: contact_us)
  end

  def edit(conn, %{"id" => id}) do
    contact_us = TodoApp.get_contact_us!(id)
    changeset = TodoApp.change_contact_us(contact_us)
    render(conn, "edit.html", contact_us: contact_us, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contact_us" => contact_us_params}) do
    contact_us = TodoApp.get_contact_us!(id)

    case TodoApp.update_contact_us(contact_us, contact_us_params) do
      {:ok, contact_us} ->
        conn
        |> put_flash(:info, "Contact us updated successfully.")
        |> redirect(to: Routes.contact_us_path(conn, :show, contact_us))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contact_us: contact_us, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contact_us = TodoApp.get_contact_us!(id)
    {:ok, _contact_us} = TodoApp.delete_contact_us(contact_us)

    conn
    |> put_flash(:info, "Contact us deleted successfully.")
    |> redirect(to: Routes.contact_us_path(conn, :index))
  end
end
