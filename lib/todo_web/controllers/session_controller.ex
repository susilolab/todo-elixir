defmodule TodoWeb.SessionController do
  use TodoWeb, :controller

  alias Todo.Accounts
  alias Todo.Login

  def new(conn, _params) do
    changeset = %Login{} |> Login.changeset(%{})
    conn
    |> put_layout("login.html")
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"login" => auth_params}) do
    changeset = %Login{} |> Login.changeset(auth_params) 

    if changeset.valid? do
      user = Accounts.get_by_username(auth_params["username"])
      case Bcrypt.check_pass(user, auth_params["password"]) do
        {:ok, user} ->
          conn
          |> put_session(:current_user_id, user.id)
          |> put_flash(:info, "Signed in successfully.")
          |> redirect(to: Routes.todos_path(conn, :index))

        {:error, _} ->
          changeset = %{changeset | action: :create}
          
          conn
          |> put_flash(:error, "Ada masalah dengan username/password Anda")
          |> put_layout("login.html")
          |> render("new.html", changeset: changeset)
      end
    else
      changeset = %{changeset | action: :create}
      conn
      |> put_layout("login.html")
      |> render("new.html", changeset: changeset)
    end
  end

  def delete(conn, _params) do
    conn
    |> delete_session(:current_user_id)
    |> put_flash(:info, "Signed out successfully.")
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
