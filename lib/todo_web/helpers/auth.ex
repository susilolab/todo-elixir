defmodule Todo.Helpers.Auth do
  def signed_in?(conn) do
    user_id = Plug.Conn.get_session(conn, :current_user_id)
    if user_id, do: !!Todo.Repo.get(Todo.Accounts.User, user_id)
  end

  def user_id(conn) do
    _id = Plug.Conn.get_session(conn, :current_user_id)
  end
end