defmodule TodoWeb.Router do
  use TodoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/v1", TodoWeb do
    pipe_through :api  

    get "/image", ImageController, :index
    post "/image", ImageController, :create
  end

  scope "/", TodoWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/about", PageController, :about
    get "/export-xls/:id", TodosController, :export_xls
    resources "/todo", TodosController
    resources "/contact-us", ContactUsController
    resources "/signup", UserController, only: [:create, :new]
    resources "/category", CategoryController
    get "/myprofile", UserController, :profile
    get "/myprofile/edit/:id", UserController, :edit
    post "/myprofile/edit/:id", UserController, :update_photo
    get "/signin", SessionController, :new
    post "/signin", SessionController, :create
    delete "/signout", SessionController, :delete

    live "/thermostat", ThermoStatView
  end

  # Other scopes may use custom stacks.
  # scope "/api", TodoWeb do
  #   pipe_through :api
  # end
end
